defmodule GifGenerator.MixProject do
  use Mix.Project

  def project do
    [
      app: :gif_generator,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env)
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :giphy_ex, :maru]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:giphy_ex, "~> 0.1.0"},
      {:maru, "~> 0.11.3"},
      {:plug_cowboy, "~> 1.0"}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
